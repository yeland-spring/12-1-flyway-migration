CREATE TABLE client (
    `id` BIGINT PRIMARY KEY ,
    `fullName` VARCHAR(128) NOT NULL ,
    `abbreviation` VARCHAR(6) NOT NULL
)