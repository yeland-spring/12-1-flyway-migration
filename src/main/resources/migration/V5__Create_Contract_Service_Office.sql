CREATE TABLE contractServiceOffice (
    `officeId` BIGINT NOT NULL ,
    `contractId` BIGINT NOT NULL ,
    PRIMARY KEY (officeId, contractId),
    FOREIGN KEY (officeId) REFERENCES office(id),
    FOREIGN KEY (contractId) REFERENCES contract(id)
)

