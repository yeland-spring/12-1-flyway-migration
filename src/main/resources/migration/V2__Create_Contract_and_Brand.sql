CREATE TABLE brand (
    `id` BIGINT AUTO_INCREMENT PRIMARY KEY ,
    `clientId` BIGINT NOT NULL ,
    `name` VARCHAR(100) NOT NULL ,
    FOREIGN KEY (clientId) REFERENCES client (id)
);

CREATE TABLE contract (
    `id` BIGINT AUTO_INCREMENT PRIMARY KEY ,
    `brandId` BIGINT NOT NULL ,
    `name` VARCHAR(128) NOT NULL ,
    FOREIGN KEY (brandId) REFERENCES brand (id)
)