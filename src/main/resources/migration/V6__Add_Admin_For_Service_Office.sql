ALTER TABLE contractServiceOffice RENAME contract_service_office;

ALTER TABLE contract_service_office ADD staffId BIGINT NOT NULL ;

ALTER TABLE contract_service_office ADD FOREIGN KEY (staffId) REFERENCES staff(id);