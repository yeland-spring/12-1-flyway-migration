CREATE TABLE office (
    `id` BIGINT AUTO_INCREMENT PRIMARY KEY ,
    `country` VARCHAR(100) NOT NULL ,
    `city` varchar(100) NOT NULL
);

CREATE TABLE staff (
    `id` BIGINT AUTO_INCREMENT PRIMARY KEY ,
    `officeId` BIGINT NOT NULL ,
    `firstName` VARCHAR(100) NOT NULL ,
    `lastName` VARCHAR(100) NOT NULL ,
    FOREIGN KEY (officeId) REFERENCES office(id)
)