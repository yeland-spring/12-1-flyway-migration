CREATE TABLE service (
    `id` BIGINT AUTO_INCREMENT PRIMARY KEY ,
    `description` VARCHAR(100) NOT NULL
);

ALTER TABLE contract ADD `serviceId` BIGINT NOT NULL ;

ALTER TABLE contract ADD FOREIGN KEY (serviceId) REFERENCES service (id);